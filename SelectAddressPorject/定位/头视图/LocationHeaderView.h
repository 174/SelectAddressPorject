//
//  LocationHeaderView.h
//  NNHealthMall
//
//  Created by 蓓蕾 on 2021/9/2.
//  Copyright © 2021 YY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HotCityHeaderView.h"

//@class HotCityConfig;

NS_ASSUME_NONNULL_BEGIN

@interface LocationHeaderView : UITableViewHeaderFooterView

-(id)initWithReuseIdentifier:(NSString *)reuseIdentifier withConfig:(HotCityConfig *)config;

@property (nonatomic, copy) void (^SelectAreaBlock)(BOOL show);

@property (nonatomic, copy) void (^AreaViewHeightChangeBlock)(CGFloat viewHeight);

@property (nonatomic, copy) void (^CityViewBlock)(UIButton *sender, NSString *title);

@property (nonatomic, copy) void (^ResultCityBlock)(UIButton *sender, NSString *title);

@property (nonatomic, assign) BOOL show;

+(HotCityConfig *)defaultConfig;

@property (nonatomic, assign) CGFloat viewHeight;

@property (nonatomic, assign) CGFloat viewMinHeight;

@property (nonatomic, strong) NSArray *areaArr;
@property (nonatomic, strong) NSArray *locationArr;
@property (nonatomic, strong) NSArray *hotCityArr;

@property (nonatomic, strong) NSString *locationString;

-(CGFloat)getMaxViewHeight;
-(CGFloat)getMinViewHeight;

@end

NS_ASSUME_NONNULL_END
