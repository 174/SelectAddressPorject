//
//  LocationHeaderView.m
//  NNHealthMall
//
//  Created by 蓓蕾 on 2021/9/2.
//  Copyright © 2021 YY. All rights reserved.
//

#import "LocationHeaderView.h"
//#import "HotCityHeaderView.h"
#import "LocationTopView.h"

@interface LocationHeaderView ()

@property (nonatomic, strong) HotCityHeaderView *areaView;
@property (nonatomic, strong) HotCityHeaderView *hotCityView;
@property (nonatomic, strong) HotCityHeaderView *locationView;
@property (nonatomic, strong) LocationTopView *topView;

@property (nonatomic, strong) HotCityConfig *config;

//@property (nonatomic, strong) NSArray *cityArr;

@end

@implementation LocationHeaderView

-(id)initWithReuseIdentifier:(NSString *)reuseIdentifier withConfig:(HotCityConfig *)config
{
    _config = config;
    return [self initWithReuseIdentifier:reuseIdentifier];
}

-(id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        if (!_config) {
            _config = [HotCityConfig new];
            _config.leftMargin = 20;
            _config.rightMargin = 20;
            _config.topMargin = 10;
            _config.bottomMargin = 10;
            _config.itemHorizontalMargin = 10;
            _config.itemVerticalMargin = 10;
            _config.itemCount = 3;
            _config.itemHeight = 60;
        }
        _topView = [[LocationTopView alloc] init];
        [self.contentView addSubview:_topView];
        [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).mas_offset(0);
            make.right.mas_equalTo(self.contentView.mas_right).mas_offset(0);
            make.top.mas_equalTo(self.contentView.mas_top).mas_offset(0);
            make.height.mas_equalTo(50);
        }];
        
        _areaView = [[HotCityHeaderView alloc] initWithConfig:_config];
        [self.contentView addSubview:_areaView];
//        _areaView.cityArr = _cityArr;
        _areaView.showHeaderView = NO;
        _areaView.hidden = YES;
        _areaView.autoHeight = NO;
        [_areaView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).mas_offset(0);
            make.right.mas_equalTo(self.contentView.mas_right).mas_offset(0);
            make.top.mas_equalTo(_topView.mas_bottom).mas_offset(0);
            make.height.mas_equalTo(0);
        }];
        
        _locationView = [[HotCityHeaderView alloc] initWithConfig:_config];
        [self.contentView addSubview:_locationView];
//        _locationView.cityArr = _cityArr;
        _locationView.titleString = @"定位/最近访问";
        [_locationView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).mas_offset(0);
            make.right.mas_equalTo(self.contentView.mas_right).mas_offset(0);
            make.top.mas_equalTo(_areaView.mas_bottom).mas_offset(0);
        }];
        _hotCityView = [[HotCityHeaderView alloc] initWithConfig:_config];
        [self.contentView addSubview:_hotCityView];
//        _hotCityView.cityArr = _cityArr;
        _hotCityView.titleString = @"热门城市";
        [_hotCityView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_locationView.mas_left).mas_offset(0);
            make.right.mas_equalTo(_locationView.mas_right).mas_offset(0);
            make.top.mas_equalTo(_locationView.mas_bottom).mas_offset(0);
        }];
        
//        _viewHeight = 50 +  _areaView.viewHeight + _locationView.viewHeight + _hotCityView.viewHeight;
//
//        _viewMinHeight = 50 + _locationView.viewHeight + _hotCityView.viewHeight;
        
        MJWeakSelf
        _topView.SelectAreaBlock = ^(BOOL show) {
            weakSelf.show = show;
            if (show) {
                weakSelf.areaView.hidden = NO;
                CGFloat selfHeight = weakSelf.config.topMargin + ((weakSelf.areaArr.count - 1) / weakSelf.config.itemCount) * (weakSelf.config.itemHeight + weakSelf.config.itemVerticalMargin) + weakSelf.config.itemHeight + weakSelf.config.bottomMargin;
                if (weakSelf.areaArr.count == 0) {
                    selfHeight = 30;
                }
                [weakSelf.areaView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(selfHeight);
                }];
            }
            else
            {
                weakSelf.areaView.hidden = YES;
                [weakSelf.areaView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(0);
                }];
            }
            if (weakSelf.SelectAreaBlock) {
                weakSelf.SelectAreaBlock(show);
            }
        };
        
        _areaView.CityViewBlock = ^(UIButton * _Nonnull sender, NSString * _Nonnull title) {
            if (weakSelf.ResultCityBlock) {
                weakSelf.ResultCityBlock(sender, title);
            }
        };
        
        _locationView.CityViewBlock = ^(UIButton * _Nonnull sender, NSString * _Nonnull title) {
            if (weakSelf.CityViewBlock) {
                weakSelf.CityViewBlock(sender, title);
            }
        };
        
        _hotCityView.CityViewBlock = ^(UIButton * _Nonnull sender, NSString * _Nonnull title) {
            if (weakSelf.CityViewBlock) {
                weakSelf.CityViewBlock(sender, title);
            }
        };
    }
    return self;
}

+(HotCityConfig *)defaultConfig
{
    HotCityConfig *config = [HotCityConfig new];
    config.leftMargin = 20;
    config.rightMargin = 20;
    config.topMargin = 10;
    config.bottomMargin = 10;
    config.itemHorizontalMargin = 10;
    config.itemVerticalMargin = 10;
    config.itemCount = 3;
    config.itemHeight = 60;
    return config;
}

-(void)setLocationString:(NSString *)locationString
{
    _locationString = locationString;
    _topView.locationString = [NSString stringWithFormat:@"当前城市: %@",locationString];
}

-(void)setAreaArr:(NSArray *)areaArr
{
    _areaArr = areaArr;
    _areaView.cityArr = areaArr;
}

-(void)setShow:(BOOL)show
{
    _show = show;
    _areaView.autoHeight = show;
}

-(CGFloat)getMaxViewHeight
{
    NSLog(@"_areaView.viewHeight = %lf",_areaView.viewHeight);
    NSLog(@"_locationView.viewHeight = %lf",_locationView.viewHeight);
    NSLog(@"_hotCityView.viewHeight = %lf",_hotCityView.viewHeight);
//    if (<#condition#>) {
//        <#statements#>
//    }
    _viewHeight = 50 +  _areaView.viewHeight + _locationView.viewHeight + _hotCityView.viewHeight;
    return _viewHeight;
}
-(CGFloat)getMinViewHeight
{
    _viewHeight = 50 + _locationView.viewHeight + _hotCityView.viewHeight;
    return _viewHeight;
}

-(void)setLocationArr:(NSArray *)locationArr
{
    _locationArr = locationArr;
    _locationView.cityArr = locationArr;
}

-(void)setHotCityArr:(NSArray *)hotCityArr
{
    _hotCityArr = hotCityArr;
    _hotCityView.cityArr = hotCityArr;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
