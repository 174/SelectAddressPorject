//
//  HotCityHeaderView.m
//  NNHealthMall
//
//  Created by 蓓蕾 on 2021/9/2.
//  Copyright © 2021 YY. All rights reserved.
//

#import "HotCityHeaderView.h"

@implementation HotCityConfig

@end

@interface HotCityHeaderView ()

@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) UILabel *titleView;

@property (nonatomic, strong) HotCityConfig *config;

@end

@implementation HotCityHeaderView

-(id)initWithConfig:(HotCityConfig *)config
{
    _config = config;
    return [self initWithFrame:CGRectZero];
}

-(id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        if (!_config) {
            _config = [HotCityConfig new];
            _config.leftMargin = 20;
            _config.rightMargin = 20;
            _config.topMargin = 10;
            _config.bottomMargin = 10;
            _config.itemHorizontalMargin = 10;
            _config.itemVerticalMargin = 10;
            _config.itemCount = 3;
//            _config.itemWidth = (kScreenWidth - _config.leftMargin - _config.rightMargin - _config.itemHorizontalMargin * (_config.itemCount - 1)) / _config.itemCount;
            _config.itemHeight = 60;
        }
        
        _autoHeight = YES;
        _showHeaderView = YES;
        _topView = [[UIView alloc] init];
        [self addSubview:_topView];
        [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).mas_offset(0);
            make.right.mas_equalTo(self.mas_right).mas_offset(0);
            make.top.mas_equalTo(self.mas_top).mas_offset(0);
            make.height.mas_equalTo(30);
        }];
        
        _contentView = [[UIView alloc] init];
        [self addSubview:_contentView];
        [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_topView.mas_left).mas_offset(0);
            make.right.mas_equalTo(_topView.mas_right).mas_offset(0);
            make.top.mas_equalTo(_topView.mas_bottom).mas_offset(0);
            make.bottom.mas_equalTo(self.mas_bottom).mas_offset(0);
//            make.bottom.mas_lessThanOrEqualTo(self.mas_bottom).mas_offset(0);
        }];
        
        _titleView = [[UILabel alloc] init];
        _titleView.textColor = rgbColor(0x777777);
        _titleView.font = [UIFont systemFontOfSize:15];
        [_topView addSubview:_titleView];
        [_titleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_topView.mas_left).mas_offset(20);
            make.centerY.mas_equalTo(_topView.mas_centerY).mas_offset(0);
        }];
        _titleView.text = @"热门城市";
    }
    return self;
}

-(void)setCityArr:(NSArray *)cityArr
{
    _cityArr = cityArr;
    
    for (UIView *object in [_contentView subviews]) {
        [object removeFromSuperview];
    }
    
    CGFloat leftMargin = _config.leftMargin;
    CGFloat topMargin = _config.topMargin;
    CGFloat bottomMargin = _config.bottomMargin;
    CGFloat itemHorizontalMargin = _config.itemHorizontalMargin;
    CGFloat itemVerticalMargin = _config.itemVerticalMargin;
    NSInteger itemCount = _config.itemCount;
    _config.itemWidth = (kScreenWidth - _config.leftMargin - _config.rightMargin - _config.itemHorizontalMargin * (_config.itemCount - 1)) / _config.itemCount;
    CGFloat itemWidth = _config.itemWidth;
    
    CGFloat itemHeight = _config.itemHeight;
//    UIView *lastView = nil;
    
    
    
    if (cityArr.count == 0) {
        
        UIView *superView = _contentView;
        if (_showHeaderView) {
            superView = _topView;
        }
        
        UILabel *placeholderView = [[UILabel alloc] init];
        placeholderView.textColor = rgbColor(0x333333);
        placeholderView.font = [UIFont systemFontOfSize:13];
        [superView addSubview:placeholderView];
        placeholderView.text = @"暂无数据";
        [placeholderView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(superView.mas_centerX).mas_offset(0);
            make.centerY.mas_equalTo(superView.mas_centerY).mas_offset(0);
        }];
        CGFloat selfHeight = 30;
        _viewHeight = selfHeight;
        if (_autoHeight) {
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(selfHeight);
            }];
        }
        NSLog(@"_viewHeight = %lf",_viewHeight);
//        if (self.GetViewHeightBlock) {
//            self.GetViewHeightBlock(_viewHeight);
//        }
        
        
        
        
        
        
        
        return;
    }
    
    
    for (NSInteger i = 0; i < cityArr.count; ++i) {
        
//        NSLog(@"left:i %% itemCount = %ld",i % itemCount);
//        NSLog(@"top:i / itemCount = %ld",(i / itemCount));
        
        NSString *cityName = cityArr[i];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [_contentView addSubview:button];
        [button setTitle:cityName forState:UIControlStateNormal];
        [button setTitleColor:rgbColor(0x333333) forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:20];
        [button addTarget:self action:@selector(buttonMethod:) forControlEvents:UIControlEventTouchUpInside];
        button.layer.cornerRadius = 5;
        button.layer.borderColor = rgbColor(0x999999).CGColor;
        button.layer.borderWidth = 0.5;
        button.titleLabel.adjustsFontSizeToFitWidth = YES;
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_contentView.mas_left).mas_offset(leftMargin + (i % itemCount) * (itemWidth + itemHorizontalMargin));
            make.top.mas_equalTo(_contentView.mas_top).mas_offset(topMargin + (i / itemCount) * (itemHeight + itemVerticalMargin));
            make.width.mas_equalTo(itemWidth);
            make.height.mas_equalTo(itemHeight);
        }];
//        lastView = button;
    }
//    if (lastView) {
//        [_contentView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.bottom.mas_equalTo(lastView.mas_bottom).mas_offset(bottomMargin);
//        }];
//    }
    CGFloat selfHeight = 0;
    
    if (_showHeaderView) {
        selfHeight = 30 + topMargin + ((cityArr.count - 1) / itemCount) * (itemHeight + itemVerticalMargin) + itemHeight + bottomMargin;
    }
    else
    {
        selfHeight = topMargin + ((cityArr.count - 1) / itemCount) * (itemHeight + itemVerticalMargin) + itemHeight + bottomMargin;
        
        selfHeight = topMargin + ((cityArr.count - 1) / itemCount) * (itemHeight + itemVerticalMargin) + itemHeight + bottomMargin;
    }
    _viewHeight = selfHeight;
    if (_autoHeight) {
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(selfHeight);
        }];
    }
    NSLog(@"_viewHeight = %lf",_viewHeight);
//    if (self.GetViewHeightBlock) {
//        self.GetViewHeightBlock(_viewHeight);
//    }
}

-(void)buttonMethod:(UIButton *)sender
{
//    NSLog(@"text = %@",sender.titleLabel.text);
    if (self.CityViewBlock) {
        self.CityViewBlock(sender, sender.titleLabel.text);
    }
}

-(void)setTitleString:(NSString *)titleString
{
    _titleString = titleString;
    _titleView.text = titleString;
}

-(void)setShowHeaderView:(BOOL)showHeaderView
{
    _showHeaderView = showHeaderView;
    if (showHeaderView) {
        _topView.hidden = NO;
        [_topView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(30);
        }];
    }
    else
    {
        _topView.hidden = YES;
        [_topView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0);
        }];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
